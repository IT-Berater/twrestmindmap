package de.wenzlaff.mindmap;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import org.jboss.resteasy.reactive.RestResponse.StatusCode;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Test des Infoservice.
 * 
 * @author Thomas Wenzlaff
 */
@QuarkusTest
public class ReactiveInfoResourceTest {

	@Test
	public void testHelloEndpoint() {
		given().when().get("/info").then().statusCode(StatusCode.OK).body(is("1.0.0"));
	}
}