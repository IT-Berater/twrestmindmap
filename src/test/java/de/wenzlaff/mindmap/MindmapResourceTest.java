package de.wenzlaff.mindmap;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import org.jboss.resteasy.reactive.RestResponse.StatusCode;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

/**
 * Test des Mindmaps Endpoint.
 * 
 * @author Thomas Wenzlaff
 *
 */
@QuarkusTest
public class MindmapResourceTest {

	@Test
	public void testMindmapCountEndpoint() {
		given().when().get("/mindmaps/anzahl").then().statusCode(StatusCode.OK).body(is("4"));
	}

	@Test
	public void testMindmapGetEndpoint() {
		given().when().get("/mindmaps").then().statusCode(StatusCode.OK);
	}

	@Test
	public void testMindmapGetDetailsAnzahlEndpoint() {
		given().when().get("/mindmaps").then().statusCode(StatusCode.OK).body("$.size()", is(4), "[0].id", is(2), "[0].name", is("2. Mindmap"));
	}

	@Test
	public void testMindmapGetDetailsInhaltEndpoint() {
		given().when().get("/mindmaps").then().statusCode(StatusCode.OK).body("$.size()", is(4));
	}

	@Test
	public void testMindmapIdEndpoint() {
		given().when().get("/mindmaps/1").then().statusCode(StatusCode.OK).body(is("{\"id\":1,\"name\":\"Reverse Proxy\",\"erstellung\":\"2021-11-08T20:48:09.999796\"}"));
	}
}
