package de.wenzlaff.mindmap;

import io.quarkus.test.junit.NativeImageTest;

/**
 * IT Test.
 * 
 * @author Thomas Wenzlaff
 */
@NativeImageTest
public class NativeReactiveInfoResourceIT extends ReactiveInfoResourceTest {

	// Execute the same tests but in native mode.
}