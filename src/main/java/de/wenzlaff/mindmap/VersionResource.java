package de.wenzlaff.mindmap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Versionsinfo.
 * 
 * @author Thomas Wenzlaff
 */
@Path("/info")
public class VersionResource {

	private String VERSION = "1.0.0";

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getVersion() {
		return VERSION;
	}
}