package de.wenzlaff.mindmap;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import de.wenzlaff.mindmap.be.Mindmap;
import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.panache.common.Sort;
import io.smallrye.mutiny.Uni;

/**
 * http://localhost:8080/q/swagger-ui/
 * 
 * @author Thomas Wenzlaff
 */
@Path("/mindmaps")
@ApplicationScoped
public class MindmapResource {

	private static final Logger LOG = Logger.getLogger(MindmapResource.class);

	@GET
	public Uni<List<Mindmap>> get() {
		return Mindmap.listAll(Sort.by("name"));
	}

	/**
	 * Liefert die Anzahl der Mindmaps.
	 * 
	 * @return Anzahl der Mindmaps.
	 */
	@Path("/anzahl")
	@GET
	public Uni<Long> count() {
		return Mindmap.count();
	}

	@GET
	@Path("/{id}")
	public Uni<Mindmap> getSingle(Long id) {
		return Mindmap.findById(id);
	}

	@POST
	public Uni<Response> create(Mindmap mindmap) {
		LOG.info("Erzeuge Mindmap");
		mindmap.erstellung = LocalDateTime.now();
		return Panache.<Mindmap>withTransaction(mindmap::persist).onItem().transform(inserted -> Response.created(URI.create("/mindmaps/" + inserted.id)).build());
	}
}