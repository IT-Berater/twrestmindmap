package de.wenzlaff.mindmap.be;

import java.time.LocalDateTime;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;

/**
 * BE einer Mindmap
 * 
 * @author Thomas Wenzlaff
 */
@Entity
@Cacheable
public class Mindmap extends PanacheEntity {

	@Column(length = 120, unique = true)
	public String name;

	/**
	 * Zeitpunkt der Erstellung der Mindmap.
	 */
	@Column()
	public LocalDateTime erstellung;
}