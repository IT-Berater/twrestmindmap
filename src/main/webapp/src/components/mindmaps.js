import React from 'react'

const Mindmaps = ({ mindmaps }) => {
	return (
		<div>
			<center><h1>Mindmaps</h1></center>
			<table class="pf-c-table pf-m-grid-md" role="grid" id="table-basic">
				<caption>{mindmaps.length} von kleinhirn.eu</caption>
				<thead>
					<tr role="row">
						<th role="columnheader" scope="col">Erstellung</th>
						<th role="columnheader" scope="col">Titel</th>
					</tr>
				</thead>
				{mindmaps.map((mindmap) => (
					<tbody role="rowgroup">
						<tr role="row">
							<td role="cell" data-label="">{mindmap.erstellung}</td>
							<td role="cell" data-label="">{mindmap.name}</td>
						</tr>
					</tbody>
				))}
			</table>
		</div>
	)
};

export default Mindmaps
