import React, {Component} from 'react';
import Mindmaps from './components/mindmaps'

class App extends Component {
  state = {
    mindmaps: []
  }

  componentDidMount() {
    fetch('/mindmaps')
    .then(res => res.json())
    .then((data) => {
      this.setState({ mindmaps: data })
    })
    .catch(console.log)
  }

  render () {
    return (
      <Mindmaps mindmaps={this.state.mindmaps} />
    );
  }
}

export default App;